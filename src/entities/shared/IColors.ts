interface IColors {
  bg: string;
  bg_header: string;
  bg_primary: string;
  text_primary: string;
  bg_neutral: string;
  bg_row: string;
  bg_highlighted: string;
  text_header: string;
  text_placeholder: string;
  text_content: string;
  disabled: string;
  sort_icon: string;
}

export default IColors;
