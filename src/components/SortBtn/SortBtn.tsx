import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import SortIcon from '../../assets/svgs/SortIcon';
import {sortBy} from '../../entities/shared/ISort';
import {useColors} from '../../shared/colors';

export interface SortBtnProps {
  sortBy?: sortBy;
  onPress: (sortBy: sortBy) => void;
}
const SortBtn = ({sortBy, onPress}: SortBtnProps) => {
  const colors = useColors();
  const [sort, setSort] = useState<sortBy | undefined>(sortBy);

  const _onPress = () => {
    const sortVal = !sort
      ? 'ASC'
      : sort == 'ASC'
      ? 'DSC'
      : sort === 'DSC'
      ? undefined
      : 'ASC';
    setSort(sortVal);
    onPress && onPress(sortVal);
  };

  useEffect(() => {
    if (sortBy !== sort) {
      setSort(sortBy);
    }
  }, [sortBy]);

  return (
    <TouchableOpacity onPress={_onPress}>
      <SortIcon fill={colors.sort_icon} direction={sort} />
    </TouchableOpacity>
  );
};

export default SortBtn;

const styles = StyleSheet.create({});
