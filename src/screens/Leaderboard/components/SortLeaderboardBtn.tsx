import React from 'react';
import {StyleSheet} from 'react-native';
import SortBtn from '../../../components/SortBtn/SortBtn';
import {useGetPlayerSortedBy} from '../../../data/usePlayerSortedBy';
import {IRankedPlayer} from '../../../entities/player';
import ISort from '../../../entities/shared/ISort';

export interface SortLeaderboardBtnProps {
  sortData: ISort<IRankedPlayer>;
  onPress: (value: ISort<IRankedPlayer>) => void;
}
const SortLeaderboardBtn = ({onPress, sortData}: SortLeaderboardBtnProps) => {
  const sortedBy = useGetPlayerSortedBy();

  const direction =
    sortedBy?.attribute === sortData.attribute ? sortedBy.by : undefined;
  return (
    <SortBtn sortBy={direction} onPress={by => onPress({by, ...sortData})} />
  );
};

export default SortLeaderboardBtn;

const styles = StyleSheet.create({});
