import IColors from '../entities/shared/IColors';

type theme = 'light';
type Colors = {
  [key in theme]: IColors;
};

export const colors: Colors = {
  light: {
    bg: '#FFFAE6',
    bg_header: '#FBF3D5',
    bg_primary: '#FF5F00',
    bg_neutral: '#fff',
    bg_row: '#CAF4FF',
    bg_highlighted: '#5AB2FF',
    text_primary: '#FF5F00',
    text_header: '#fff',
    text_placeholder: '#D6DAC8',
    text_content: '#002379',
    disabled: '#D6DAC8',
    sort_icon: '#5AB2FF',
  },
};

export const useColors = () => {
  return colors.light;
};
