import React, {useCallback} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Animated, {BounceInUp, FlipInEasyX} from 'react-native-reanimated';
import SearchIcon from '../../assets/svgs/SearchIcon';
import {IRankedPlayer} from '../../entities/player';
import IColors from '../../entities/shared/IColors';
import {useColors} from '../../shared/colors';
import PlayerRow from './components/PlayerRow';
import SortLeaderboardBtn from './components/SortLeaderboardBtn';
import useGetLeaderboardByName from './hooks/useGetLeaderboard';

const Leaderboard = () => {
  const colors = useColors();
  const styles = dynamicStyles(colors);
  const {
    leaderboard,
    setSearchValue,
    search,
    isLoading,
    searchName,
    sort,
    renderNextPage,
  } = useGetLeaderboardByName();

  const renderItem = useCallback(
    ({item: player, index}: {item: IRankedPlayer; index: number}) => {
      const isHighlighted = player.rank === leaderboard.searchedRanked;
      return (
        <PlayerRow
          player={player}
          index={index}
          isHighlighted={isHighlighted}
        />
      );
    },
    [leaderboard.searchedRanked, searchName],
  );

  const renderHeader = useCallback(() => {
    return (
      <Animated.View
        style={[styles.row, styles.tableHeader]}
        entering={FlipInEasyX.delay(500).duration(500)}>
        <View style={[styles.col1, styles.headerCol]}>
          <Text style={styles.tableTitle}>{'Name'}</Text>
          <SortLeaderboardBtn
            sortData={{attribute: 'name', type: 'string'}}
            onPress={arg => {
              sort(arg);
            }}
          />
        </View>
        <View style={[styles.col2, styles.headerCol]}>
          <Text style={[styles.tableTitle]}>{'Rank'}</Text>
        </View>

        <View style={[styles.col3, styles.headerCol]}>
          <Text style={[styles.tableTitle]}>{'Bananas'}</Text>
          <SortLeaderboardBtn
            sortData={{attribute: 'bananas', type: 'number'}}
            onPress={arg => {
              sort(arg);
            }}
          />
        </View>
      </Animated.View>
    );
  }, []);

  return (
    <View style={{backgroundColor: colors.bg, flex: 1}}>
      <Animated.View
        entering={BounceInUp.duration(500)}
        style={styles.titleContainer}>
        <Text style={styles.title}>LEADERBOARD</Text>
      </Animated.View>

      <View style={styles.searchContainer}>
        <TextInput
          style={styles.searchInput}
          placeholderTextColor={colors.disabled}
          placeholder={'Search'}
          onChangeText={text => setSearchValue(text)}
        />
        <TouchableOpacity onPress={search}>
          <SearchIcon />
        </TouchableOpacity>
      </View>

      <View style={styles.listContainer}>
        {renderHeader()}
        <Animated.FlatList
          onEndReached={renderNextPage}
          data={isLoading ? [] : leaderboard.players}
          renderItem={renderItem}
          keyExtractor={i => i.uid}
        />
      </View>
    </View>
  );
};

export default Leaderboard;

const dynamicStyles = (colors: IColors) =>
  StyleSheet.create({
    titleContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      alignSelf: 'center',
      marginTop: 20,
      padding: 20,
      borderRadius: 30,
    },
    title: {
      fontSize: 30,
      color: colors.text_primary,
      fontWeight: '900',
    },
    searchContainer: {
      marginHorizontal: 16,
      flexDirection: 'row',
      gap: 15,
      alignItems: 'center',
    },
    searchInput: {
      flex: 1,
      color: colors.text_primary,
      paddingHorizontal: 20,
      borderWidth: 2,
      borderColor: colors.bg_primary,
      borderRadius: 20,
      fontSize: 18,
    },
    row: {
      flexDirection: 'row',
      paddingVertical: 10,
    },
    col1: {
      flex: 0.5,
    },
    col2: {
      flex: 0.2,
    },
    col3: {
      flex: 0.3,
    },
    headerCol: {
      flexDirection: 'row',
      gap: 15,
      alignItems: 'center',
    },
    listContainer: {
      flex: 1,
      borderRadius: 20,
      marginTop: 20,
      paddingBottom: 10,
      transform: [
        {rotateX: '0deg'},
        {rotateY: '0deg'},
        {rotateZ: '-3deg'},
        {translateX: 10},
      ],
      backgroundColor: 'green',
    },
    tableHeader: {
      marginHorizontal: 16,
      paddingHorizontal: 20,
      borderRadius: 20,
      paddingVertical: 10,
      backgroundColor: colors.bg_header,
      marginVertical: 10,
    },
    listHeader: {
      // transform: [{rotateZ: '-3deg'}],
    },
    tableTitle: {
      color: colors.text_primary,
      fontWeight: '500',
    },
  });
