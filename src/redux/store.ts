import {useDispatch, useSelector} from 'react-redux';
import {applyMiddleware, legacy_createStore} from 'redux';
import {thunk} from 'redux-thunk';
import {playersInitialstate} from './reducers/playerReducer';
import rootReducer from './reducers/reducer';

export const initialstate = {
  players: {
    ...playersInitialstate,
  },
};

const store = legacy_createStore(
  rootReducer,
  initialstate,
  applyMiddleware(thunk),
);

export type AppState = ReturnType<typeof store.getState>;
export const useAppDispatch = useDispatch.withTypes<typeof store.dispatch>();
export const useAppSelector = useSelector.withTypes<AppState>();

export default store;
