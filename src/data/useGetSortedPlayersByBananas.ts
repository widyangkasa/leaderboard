import {useEffect} from 'react';
import {setSortedPlayersByBanana} from '../redux/reducers/playerReducer';
import {useAppDispatch, useAppSelector} from '../redux/store';

import {useGetPlayers} from './useGetPlayers';

export const useGetSortedPlayersByBananas = () => {
  const sortedPlayersByBanana = useAppSelector(
    state => state.players.sortedPlayersByBanana,
  );
  const players = useGetPlayers();
  const dispatch = useAppDispatch();

  useEffect(() => {
    //default sort by total of bananas then by name asc
    const sortedPlayer = Object.values(players || {}).sort(
      (a, b) => b.bananas - a.bananas || a.name?.localeCompare(b.name),
    );
    dispatch(setSortedPlayersByBanana(sortedPlayer));
  }, [players]);

  return sortedPlayersByBanana;
};
