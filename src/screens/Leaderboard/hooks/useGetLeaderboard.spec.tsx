import {renderHook} from '@testing-library/react-hooks';

import * as useGetSortedPlayersByBananas from '../../../data/useGetSortedPlayersByBananas';
import * as constants from '../../../shared/constants';
import {waitFor} from '../../../shared/utils';
import {getReduxWrapper} from '../../../test/redux';
import useGetLeaderboardByName from './useGetLeaderboard';

describe('useGetLeaderboard', () => {
  beforeEach(() => {});

  it('should render hook', () => {
    const wrapper = getReduxWrapper();
    const {result} = renderHook(() => useGetLeaderboardByName(), {wrapper});
    expect(result.current).toBeDefined();
  });

  it('empty search and no sort selected, should sort dsc by bananas', () => {
    const testData = test_data.data1;
    jest
      .spyOn(useGetSortedPlayersByBananas, 'useGetSortedPlayersByBananas')
      .mockReturnValue(testData);
    const wrapper = getReduxWrapper();
    const {result} = renderHook(() => useGetLeaderboardByName(), {wrapper});
    result.current.leaderboard.players.map((player, idx) =>
      expect(player.uid).toEqual(testData[idx].uid),
    );
  });

  it('if search player exist, should appear in the top group', async () => {
    // @ts-ignore
    constants.totalSearchedLeaderboardPlayer = 3;

    const testData = test_data.data1;
    jest
      .spyOn(useGetSortedPlayersByBananas, 'useGetSortedPlayersByBananas')
      .mockReturnValue(testData);

    const wrapper = getReduxWrapper();
    const {result} = renderHook(() => useGetLeaderboardByName(), {wrapper});

    //user that not exist in top group
    result.current.setSearchValue('Adh');
    result.current.search();
    await waitFor(200);
    const resultData = [testData[0], testData[1], testData[3]];
    result.current.leaderboard.players.map((player, idx) =>
      expect(player.uid).toEqual(resultData[idx].uid),
    );

    //user that exist in top group
    result.current.setSearchValue('Yumi');
    result.current.search();
    await waitFor(200);
    const resultData2 = [testData[0], testData[1], testData[2]];
    result.current.leaderboard.players.map((player, idx) =>
      expect(player.uid).toEqual(resultData2[idx].uid),
    );
  });

  it('select sort bananas by asc, should sort player asc by bananas and players with the same score are listed alphabetically', async () => {
    const testData = test_data.data1;
    jest
      .spyOn(useGetSortedPlayersByBananas, 'useGetSortedPlayersByBananas')
      .mockReturnValue(testData);
    const wrapper = getReduxWrapper();
    const {result} = renderHook(() => useGetLeaderboardByName(), {wrapper});

    result.current.sort({
      attribute: 'bananas',
      type: 'number',
      by: 'ASC',
    });
    await waitFor(200);

    const resultData = [testData[3], testData[1], testData[2], testData[0]];
    result.current.leaderboard.players.map((player, idx) =>
      expect(player.uid).toEqual(resultData[idx].uid),
    );
  });
});

const test_data: Record<
  string,
  ReturnType<typeof useGetSortedPlayersByBananas.useGetSortedPlayersByBananas>
> = {
  data1: [
    {
      bananas: 200,
      lastDayPlayed: '2018-11-22',
      longestStreak: 1,
      name: 'Rica Ella Francisco',
      stars: 6,
      subscribed: false,
      uid: '00D1LA8puAa1GINkVpfgC1TmO0m1',
    },
    {
      bananas: 150,
      lastDayPlayed: '2018-10-17',
      longestStreak: 1,
      name: 'Jayne Su YueHe',
      stars: 4,
      subscribed: false,
      uid: 'ylL3XqPOlycHiPBuf1uXHlgZzEr3',
    },
    {
      bananas: 150,
      lastDayPlayed: '2018-10-17',
      longestStreak: 1,
      name: 'Yumi',
      stars: 4,
      subscribed: false,
      uid: 'ylL3XqPOlycHiPBuf1uXHlgZzEr2',
    },
    {
      bananas: 0,
      lastDayPlayed: '2017-11-01',
      longestStreak: 0,
      name: 'Adh Fuoo',
      stars: 4,
      subscribed: false,
      uid: 'x8RNvUgv5pZqDVatEXb2aYgSflq1',
    },
  ],
};
