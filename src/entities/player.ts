export default interface IPlayer {
  bananas: number;
  lastDayPlayed: string;
  longestStreak: number;
  name: string;
  stars: number;
  subscribed: boolean;
  uid: string;
}

export interface IRankedPlayer extends IPlayer {
  rank: number;
}
