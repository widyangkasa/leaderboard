interface ISort<T> {
  attribute: keyof T;
  type: 'string' | 'number';
  by?: sortBy;
}

export type sortBy = 'ASC' | 'DSC';

export default ISort;
