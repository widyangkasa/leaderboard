import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Animated, {SlideInLeft} from 'react-native-reanimated';
import BadgeIcon from '../../../assets/svgs/BadgeIcon';
import {IRankedPlayer} from '../../../entities/player';
import IColors from '../../../entities/shared/IColors';
import {useColors} from '../../../shared/colors';
import {totalLeaderboardPlayerPerPage} from '../../../shared/constants';

export interface PlayerRowProps {
  player: IRankedPlayer;
  isHighlighted: boolean;
  index: number;
}

const PlayerRow = ({player, isHighlighted, index}: PlayerRowProps) => {
  const colors = useColors();
  const styles = dynamicStyles(colors);
  return (
    <Animated.View
      style={[
        styles.row,
        styles.playerRow,
        isHighlighted ? styles.highlightedRow : {},
      ]}
      entering={SlideInLeft.delay(
        ((index % totalLeaderboardPlayerPerPage) + 1) * 50,
      ).duration(500)}>
      <Text style={[styles.col1, styles.name]}>
        {player.name || 'Anonymous'}
      </Text>

      <View style={[styles.col2, styles.rankRow]}>
        <BadgeIcon
          fill={
            player.rank === 1
              ? '#DDB558'
              : player.rank === 2
              ? '#C4C3C3'
              : player.rank === 3
              ? '#D88C64'
              : colors.disabled
          }
        />
        <Text
          style={[
            styles.rank,
            isHighlighted ? styles.highlightedRowTitle : {},
          ]}>
          {player.rank}
        </Text>
      </View>

      <Text
        style={[
          styles.col3,
          styles.bananas,
          isHighlighted ? styles.highlightedRowTitle : {},
        ]}>
        {player.bananas}
      </Text>
    </Animated.View>
  );
};

export default PlayerRow;

const dynamicStyles = (colors: IColors) =>
  StyleSheet.create({
    playerRow: {
      marginHorizontal: 16,
      marginVertical: 0.5,
      paddingHorizontal: 20,
      backgroundColor: colors.bg_row,
      borderRadius: 30,
      alignItems: 'center',
      // transform: [{rotateZ: '-3deg'}],
    },

    row: {
      flexDirection: 'row',
      paddingVertical: 10,
    },
    name: {
      color: colors.text_content,
    },
    rank: {
      color: colors.text_primary,
      textAlign: 'center',
      fontSize: 20,
    },
    rankRow: {
      flexDirection: 'row',
    },
    bananas: {
      color: colors.text_primary,
    },
    col1: {
      flex: 0.5,
    },
    col2: {
      flex: 0.2,
    },
    col3: {
      flex: 0.3,
    },
    highlightedRow: {
      backgroundColor: colors.bg_primary,
      transform: [{scale: 1.03}],
    },
    highlightedRowTitle: {
      color: colors.text_content,
    },
  });
