import {useMemo, useRef, useState} from 'react';
import {Keyboard} from 'react-native';
import {useGetSortedPlayersByBananas} from '../../../data/useGetSortedPlayersByBananas';
import {useSetPlayerSortedBy} from '../../../data/usePlayerSortedBy';
import IPlayer, {IRankedPlayer} from '../../../entities/player';
import ISort from '../../../entities/shared/ISort';
import {
  totalLeaderboardPlayerPerPage,
  totalSearchedLeaderboardPlayer,
} from '../../../shared/constants';

const useGetLeaderboardByName = () => {
  const players = useGetSortedPlayersByBananas();
  const searchValueRef = useRef<string>('');
  const [searchName, setSearchName] = useState<string>('');

  const [sortList, setSortList] = useState<ISort<IRankedPlayer>[]>([]);
  const {setSortedBy} = useSetPlayerSortedBy();
  const savedSortedPlayer = useRef<Record<string, IRankedPlayer[]>>();

  const [displayPage, setDisplayPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isShowNotExistModal, setIsShowNotExistModal] =
    useState<boolean>(false);

  const showNotExistModal = () => {
    alert(
      'This user name does not exist! Please specify an existing user name!',
    );
  };

  const setSearchValue = (text: string) => {
    searchValueRef.current = text;
  };

  const search = () => {
    Keyboard.dismiss();
    const val = searchValueRef.current.trim();

    setIsLoading(true);
    resetSort();
    setTimeout(() => {
      setSearchName(val);
      setIsLoading(false);
    }, 100);
  };

  const resetSort = () => {
    setSortedBy(undefined);
    setSortList([]);
    setDisplayPage(1);
  };

  const sort = (arg: ISort<IRankedPlayer>) => {
    setIsLoading(true);
    setDisplayPage(1);
    setSortedBy(arg.by ? (arg as ISort<IPlayer>) : undefined);
    setTimeout(() => {
      if (arg.by) {
        setSortList([arg]);
      } else {
        setSortList([]);
      }
      setIsLoading(false);
    }, 100);
  };

  const defaultLeaderboard: IRankedPlayer[] = useMemo(() => {
    savedSortedPlayer.current = {};
    return players.map((p, idx) => ({...p, rank: idx + 1}));
  }, [players]);

  const searchedleaderboard = useMemo(() => {
    let result = [...defaultLeaderboard];
    let searchedPlayerIndex = -1;

    if (searchName) {
      searchedPlayerIndex = players.findIndex(player =>
        player.name?.toLowerCase().includes(searchName.toLowerCase()),
      );
      if (searchedPlayerIndex < 0) {
        showNotExistModal();
        setSearchName('');
      } else if (searchedPlayerIndex < totalSearchedLeaderboardPlayer) {
        result = defaultLeaderboard.slice(0, totalSearchedLeaderboardPlayer);
      } else if (searchedPlayerIndex + 1 >= totalSearchedLeaderboardPlayer) {
        result = [
          ...defaultLeaderboard.slice(0, totalSearchedLeaderboardPlayer - 1),
          {
            ...players[searchedPlayerIndex],
            rank: searchedPlayerIndex + 1,
          },
        ];
      }
    }

    if (sortList.length > 0) {
      const sortItem = sortList[0];
      const key = `${sortItem.attribute}_${sortItem.by}`;
      const savedResult = savedSortedPlayer.current?.[key];
      if (result.length === players.length && savedResult) {
        result = [...savedResult];
      } else {
        result = result.sort((x, y) => sortByNameOrBananas(x, y, sortItem));
        result.length === players.length &&
          (savedSortedPlayer.current[key] = result);
      }
    }

    return {
      searchedRanked: searchedPlayerIndex + 1,
      players: result,
    };
  }, [defaultLeaderboard, searchName, sortList]);

  const displayedLeaderboard = useMemo(() => {
    return {
      ...searchedleaderboard,
      players: searchedleaderboard.players.slice(
        0,
        displayPage * totalLeaderboardPlayerPerPage,
      ),
    };
  }, [searchedleaderboard, displayPage]);

  const renderNextPage = () => {
    setDisplayPage(prev => prev + 1);
  };

  return {
    leaderboard: displayedLeaderboard,
    search,
    searchName,
    setSearchValue,
    isLoading,
    sort,
    renderNextPage,
  };
};

const sortByNameOrBananas = (
  x: IRankedPlayer,
  y: IRankedPlayer,
  sortItem: ISort<IRankedPlayer>,
) => {
  const a = sortItem.by === 'ASC' ? x : y;
  const b = sortItem.by === 'ASC' ? y : x;

  if (sortItem.attribute === 'name') {
    return (a[sortItem.attribute] as string)?.localeCompare(
      b[sortItem.attribute] as string,
    );
  } else {
    const attr2 = 'name';
    return (
      (a[sortItem.attribute] as number) - (b[sortItem.attribute] as number) ||
      (x[attr2] as string)?.localeCompare(y[attr2] as string) //name to always sort by asc
    );
  }
};

// const compareMultipleSort = (
//   x: IRankedPlayer,
//   y: IRankedPlayer,
//   sortList: ISort<IRankedPlayer>[],
// ) => {
//   for (let sortItem of sortList) {
//     const a = sortItem.by === 'ASC' ? x : y;
//     const b = sortItem.by === 'ASC' ? y : x;

//     if (sortItem.type === 'number') {
//       const c =
//         (a[sortItem.attribute] as number) - (b[sortItem.attribute] as number);
//       if (c === 0) {
//         continue;
//       } else {
//         return c;
//       }
//     } else if (sortItem.type === 'string') {
//       const c = (a[sortItem.attribute] as string)?.localeCompare(
//         b[sortItem.attribute] as string,
//       );
//       if (c === 0) {
//         continue;
//       } else {
//         return c;
//       }
//     }
//   }
//   return 0;
// };

export default useGetLeaderboardByName;
