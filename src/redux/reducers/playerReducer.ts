import IPlayer from '../../entities/player';
import ISort from '../../entities/shared/ISort';

export const SET_PLAYERS = 'SET_PLAYERS';
export const SET_SORTED_PLAYERS_BY_BANANA = 'SET_SORTED_PLAYERS_BY_BANANA';
export const SET_SORT_BY = 'SET_SORT_BY';

export const setPlayers = (payload: Record<string, IPlayer>) => ({
  type: SET_PLAYERS,
  payload,
});

export const setSortedPlayersByBanana = (payload: IPlayer[]) => ({
  type: SET_SORTED_PLAYERS_BY_BANANA,
  payload,
});

export const setSortedBy = (payload: ISort<IPlayer>) => ({
  type: SET_SORT_BY,
  payload,
});

type PlayerState = {
  players: Record<string, IPlayer>;
  sortedPlayersByBanana: IPlayer[];
  sortedBy?: ISort<IPlayer>;
};

export const playersInitialstate: PlayerState = {
  players: {},
  sortedPlayersByBanana: [],
  sortedBy: undefined,
};

type Action = {
  type: string;
  payload?: any;
};

const playerReducer = (
  state: PlayerState = playersInitialstate,
  action: Action,
): PlayerState => {
  switch (action.type) {
    case SET_PLAYERS:
      return {
        ...state,
        players: action.payload,
      };
    case SET_SORTED_PLAYERS_BY_BANANA:
      return {
        ...state,
        sortedPlayersByBanana: action.payload,
      };
    case SET_SORT_BY:
      return {
        ...state,
        sortedBy: action.payload,
      };
    default:
      return state;
  }
};

export default playerReducer;
