import {setSortedBy} from '../redux/reducers/playerReducer';
import {useAppDispatch, useAppSelector} from '../redux/store';

import IPlayer from '../entities/player';
import ISort from '../entities/shared/ISort';

export const useGetPlayerSortedBy = () => {
  const sortedBy = useAppSelector(state => state.players.sortedBy);
  return sortedBy;
};

export const useSetPlayerSortedBy = () => {
  const dispatch = useAppDispatch();
  const _setSortedBy = (arg: ISort<IPlayer>) => {
    dispatch(setSortedBy(arg));
  };
  return {
    setSortedBy: _setSortedBy,
  };
};
