import IPlayer from '../../entities/player';

export const SET_LEADERBOARD = 'SET_LEADERBOARD';

export const setLeaderboard = (payload: IPlayer[]) => ({
  type: SET_LEADERBOARD,
  payload,
});

type LeaderboardState = {
  leaderboard: IPlayer[];
};

export const leaderboardInitialstate: LeaderboardState = {
  leaderboard: [],
};

type Action = {
  type: string;
  payload?: any;
};

const leaderboardReducer = (
  state: LeaderboardState = leaderboardInitialstate,
  action: Action,
): LeaderboardState => {
  switch (action.type) {
    case SET_LEADERBOARD:
      return {
        ...state,
        leaderboard: action.payload,
      };
    default:
      return state;
  }
};

export default leaderboardReducer;
