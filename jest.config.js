module.exports = {
  preset: 'react-native',
  "transform": {
    "^.+\\.(js|ts)$": "babel-jest"
  },
  "transformIgnorePatterns": [],
  "collectCoverage": true,
  "coverageReporters": ["json", "html"],
};
