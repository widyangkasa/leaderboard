import React from 'react';
import {StyleSheet} from 'react-native';
import {Path, Svg, SvgProps} from 'react-native-svg';

const SearchIcon = ({width = 24, height = 24, fill}: SvgProps) => {
  return (
    <Svg height={height} viewBox="0 -960 960 960" width={width} fill="#5f6368">
      <Path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z" />
    </Svg>
  );
};

export default SearchIcon;

const styles = StyleSheet.create({});
