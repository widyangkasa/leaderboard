import React from 'react';
import {Provider} from 'react-redux';
import {applyMiddleware, legacy_createStore} from 'redux';
import {thunk} from 'redux-thunk';
import rootReducer from '../redux/reducers/reducer';
import store, {AppState, initialstate} from '../redux/store';

interface TestProviderProps {
  children?: React.ReactNode | React.ReactNode[];
  preloadedState?: AppState;
  store?: typeof store;
}

export const createStore = (
  preloadedState: AppState,
) => legacy_createStore(rootReducer, preloadedState, applyMiddleware(thunk));

export const TestProvider = ({
  children,
  preloadedState = initialstate,
  store,
}: TestProviderProps) => {
  const testStore = store || createStore(preloadedState);
  return <Provider store={testStore}>{children}</Provider>;
};

export const getReduxWrapper =
  (store?: ReturnType<typeof createStore>) =>
  ({children}: {children?: React.ReactNode}) =>
    <TestProvider store={store}>{children}</TestProvider>;
