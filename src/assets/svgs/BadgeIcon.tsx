import React from 'react';
import {StyleSheet} from 'react-native';
import {Path, Svg, SvgProps} from 'react-native-svg';

const BadgeIcon = ({width = 24, height = 24, fill}: SvgProps) => {
  return (
    <Svg height={height} viewBox="0 -960 960 960" width={width} fill={fill}>
      <Path d="M280-880h400v314q0 23-10 41t-28 29l-142 84 28 92h152l-124 88 48 152-124-94-124 94 48-152-124-88h152l28-92-142-84q-18-11-28-29t-10-41v-314Zm80 80v234l80 48v-282h-80Zm240 0h-80v282l80-48v-234ZM480-647Zm-40-12Zm80 0Z" />
    </Svg>
  );
};

export default BadgeIcon;

const styles = StyleSheet.create({});
