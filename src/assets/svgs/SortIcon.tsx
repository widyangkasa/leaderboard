import React from 'react';
import {StyleSheet} from 'react-native';
import {Path, Svg, SvgProps} from 'react-native-svg';
import {sortBy} from '../../entities/shared/ISort';

export interface SortIconProps extends SvgProps {
  direction?: sortBy;
}

const SortIcon = ({
  width = 24,
  height = 24,
  fill,
  direction,
}: SortIconProps) => {
  return (
    <Svg height={height} viewBox="0 -960 960 960" width={width} fill={fill}>
      <Path
        d="M320-440v-287L217-624l-57-56 200-200 200 200-57 56-103-103v287h-80Z"
        opacity={direction === 'ASC' ? 1 : 0.5}
      />
      <Path
        d="M600-80 400-280l57-56 103 103v-287h80v287l103-103 57 56L600-80Z"
        opacity={direction === 'DSC' ? 1 : 0.5}
      />
    </Svg>
  );
};

export default SortIcon;

const styles = StyleSheet.create({});
