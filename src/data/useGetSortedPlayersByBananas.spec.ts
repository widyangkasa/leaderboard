import {renderHook} from '@testing-library/react-hooks';
import {getReduxWrapper} from '../test/redux';
import * as useGetPlayers from './useGetPlayers';
import * as playerHooks from './useGetSortedPlayersByBananas';

describe('useGetSortedPlayersByBananas', () => {
  it('should render hook', () => {
    const testData = test_data.data1;
    jest.spyOn(useGetPlayers, 'useGetPlayers').mockReturnValue(testData);
    const wrapper = getReduxWrapper();
    const {result} = renderHook(
      () => playerHooks.useGetSortedPlayersByBananas(),
      {
        wrapper,
      },
    );
    expect(result.current).toBeDefined();
    const testDataArr = Object.values(testData);
    const expectedReturn = [
      testDataArr[1],
      testDataArr[0],
      testDataArr[3],
      testDataArr[2],
    ];
    result.current.map((player, idx) =>
      expect(player.uid).toEqual(expectedReturn[idx].uid),
    );
  });
});

const test_data: Record<
  string,
  ReturnType<typeof useGetPlayers.useGetPlayers>
> = {
  data1: {
    ylL3XqPOlycHiPBuf1uXHlgZzEr3: {
      bananas: 150,
      lastDayPlayed: '2018-10-17',
      longestStreak: 1,
      name: 'Jayne Su YueHe',
      stars: 4,
      subscribed: false,
      uid: 'ylL3XqPOlycHiPBuf1uXHlgZzEr3',
    },
    '00D1LA8puAa1GINkVpfgC1TmO0m1': {
      bananas: 200,
      lastDayPlayed: '2018-11-22',
      longestStreak: 1,
      name: 'Rica Ella Francisco',
      stars: 6,
      subscribed: false,
      uid: '00D1LA8puAa1GINkVpfgC1TmO0m1',
    },
    x8RNvUgv5pZqDVatEXb2aYgSflq1: {
      bananas: 0,
      lastDayPlayed: '2017-11-01',
      longestStreak: 0,
      name: 'Adh Fuoo',
      stars: 4,
      subscribed: false,
      uid: 'x8RNvUgv5pZqDVatEXb2aYgSflq1',
    },
    ylL3XqPOlycHiPBuf1uXHlgZzEr2: {
      bananas: 150,
      lastDayPlayed: '2018-10-17',
      longestStreak: 1,
      name: 'Yumi',
      stars: 4,
      subscribed: false,
      uid: 'ylL3XqPOlycHiPBuf1uXHlgZzEr2',
    },
  },
};
