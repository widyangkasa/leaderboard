import {useEffect} from 'react';
import {setPlayers} from '../redux/reducers/playerReducer';
import {useAppDispatch, useAppSelector} from '../redux/store';

import * as data from './leaderboard.json';

export const useGetPlayers = () => {
  const players = useAppSelector(state => state.players.players);
  const dispatch = useAppDispatch();
  const fetchPlayer = async () => {
    try {
      dispatch(setPlayers(data));
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchPlayer();
  }, []);

  return players;
};
